/*
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
	  byte anzahlPlaneten = 9 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
      long anzahlSterne = 4000000000l;
    
    // Wie viele Einwohner hat Berlin?
       double bewohnerBerlin = 3.64;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       short alterTage = 6570;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm =   200000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
       double flaecheGroessteLand = 17.13; 
    
    // Wie gro� ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Anzahl Einwohner Berlin: " + bewohnerBerlin+"Mio");
    System.out.println("Alter in Tage: " + alterTage);
    System.out.println("Schwerste Tier der Erde: " + gewichtKilogramm+"kg");
    System.out.println("Fl�che gr��tes Land: " + flaecheGroessteLand +"Mio km�");
    System.out.println("Fl�che kleinstes Land: " + flaecheKleinsteLand +" km�");
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}