﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       float zuZahlenderBetrag = 0; 
       float eingezahlterGesamtbetrag;
       float rückgabebetrag;
       String eingabe = "";
       boolean input = true;
       float[] Fahrkartenpreise = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		Fahrkartenpreise[0] = 2.90f;
		Fahrkartenpreise[1] = 3.30f;
		Fahrkartenpreise[2] = 3.60f;
		Fahrkartenpreise[3] = 1.90f;
		Fahrkartenpreise[4] = 8.60f;
		Fahrkartenpreise[5] = 9f;
		Fahrkartenpreise[6] = 9.60f;
		Fahrkartenpreise[7] = 23.50f;
		Fahrkartenpreise[8] = 24.30f;
		Fahrkartenpreise[9] = 24.90f;
		
		String[] Fahrkartennamen = {"Einzelfahrschein Berlin AB",
									"Einzelfahrschein Berlin BC",
									"Einzelfahrschein Berlin ABC",
									"Kurzstrecke",
									"Tageskarte Berlin AB",
									"Tageskarte Berlin BC",
									"Tageskarte Berlin ABC",
									"Kleingruppen-Tageskarte Berlin AB",
									"Kleingruppen-Tageskarte Berlin BC",
									"Kleingruppen-Tageskarte Berlin ABC"};
		Scanner tastatur = new Scanner(System.in);
       do{
    	   input = true;
       zuZahlenderBetrag = Methoden.fahrkartenbestellungErfassen(Fahrkartenpreise, Fahrkartennamen, tastatur);
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0f;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   eingezahlterGesamtbetrag = Methoden.fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, tastatur);
       }

       // Fahrscheinausgabe
       // -----------------
       Methoden.farhkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       Methoden.rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag, rückgabebetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n");
       try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       while(input == true){
       System.out.println("Geben Sie 'Beenden' ein, um das Progarmm zu beenden.");
       System.out.println("Geben Sie 'Weiter' ein, um das Progarmm erneut zu nutzen.");
       eingabe = tastatur.next();

       switch(eingabe) {
       case "Beenden":
    	   eingabe = "Beenden";
    	   System.out.println("Das Programm wird jetzt geschlossen.");
    	   System.exit(1);
       case "Weiter":
    	   eingabe = "Weiter";
    	   input = false;
    	   break;
       default:  
    		System.out.println("Falsche Eingabe");
       }
       }
       }while(eingabe.equals("Weiter"));
    }
}
/*	Begründen Sie Ihre Entscheidung für die Wahl des Datentyps.
	Es wurde Byte gewählt, da davon ausgegangen werden kann , dass niemand zugleich mehr als 127 Fahrkarten erwerben möchte.

	Erläutern Sie detailliert, was bei der Berechnung des Ausdrucks anzahl * einzelpreis passiert.
	Mit diesem Ausdruck wird der Gesamtpreis berechnet, den der Kunde für eine bestimmte Anzahl an Tickets zahlen muss.
*/