import java.util.Scanner;

public class Methoden {
	
	/*public static float fahrkartenbestellungErfassen() {
		 
		 Scanner tastatur = new Scanner(System.in);
		 float zuZahlenderBetrag = 0;
		 byte ticketanzahl = 0;
		 
		   System.out.print("Zu zahlender Betrag (EURO): ");
	       zuZahlenderBetrag = tastatur.nextFloat();
	       System.out.print("Anzahl Fahrkarten: ");
	       ticketanzahl = tastatur.nextByte();
	       while(ticketanzahl <= 0 || ticketanzahl > 10)
	       {
	    	   System.out.println("Sie k�nnen nur minimal 1, maximal 10 Tickets kaufen.\nBitte geben Sie eine g�ltige Ticketanzahl an!");
	    	   System.out.println("Anzahl Fahrkarten: ");
	    	   ticketanzahl = tastatur.nextByte();
	       }
	       zuZahlenderBetrag = zuZahlenderBetrag * ticketanzahl;
	       
		return zuZahlenderBetrag;
	}*/
	
	
	
	
	public static float fahrkartenBezahlen(float eingezahlterGesamtbetrag, float zuZahlenderBetrag, Scanner tastatur) {
		//Scanner tastatur = new Scanner(System.in);
		System.out.printf("%s%.2f%s\n" , "Noch zu zahlen: ", zuZahlenderBetrag - eingezahlterGesamtbetrag, " Euro");
 	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
 	   float eingeworfeneMuenze;
 	   eingeworfeneMuenze = tastatur.nextFloat();
 	   eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMuenze;
 	   //tastatur.close();
		return eingezahlterGesamtbetrag;
	}
	
	
	
	
	public static void farhkartenAusgeben()
	{
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\nFahreschein ausgegeben");
	       System.out.println("\n");	
	}
	
	public static void rueckgeldAusgeben(float eingezahlterGesamtbetrag, float zuZahlenderBetrag, float r�ckgabebetrag)
	{
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("%s%.2f%s\n" ,"Der R�ckbetrag in H�he von ", r�ckgabebetrag, " EURO");
	    	   //System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.09) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.04)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }
	}
	
	
	public static float fahrkartenbestellungErfassen(float a[], String b[], Scanner tastatur) {

		 float zuZahlenderBetrag = 0;
		 byte auswahl = 1;
	       System.out.println("Welche Fahrkarte m�chten Sie?: ");
	       for(int i = 0; i < 10; i++)
	       {
	       System.out.printf("%-4d%-40s%9.2f%s\n", i+1, b[i], a[i], "�");
	       };
	       System.out.print("\nGeben Sie ein, welches Ticket Sie haben wollen (1-10)."
	       		+ "\nWenn Sie alle Tickets haben, geben Sie 0 ein.\n");
	       
	       System.out.println("Auswahl: ");
	       while(auswahl >= 1 || auswahl <= 9) {
	       try {
	       auswahl = tastatur.nextByte();
	       if(auswahl >= 1 && auswahl <= 10)
	       {
	    	   zuZahlenderBetrag += a[auswahl-1];
	    	   System.out.printf("%s%.2f%s\n", "Gesamtpreis: ", zuZahlenderBetrag, "�");
	       }else{
	    	   System.out.println("Sie haben kein Ticket ausgew�hlt. Bitte auf weitere Anweisungen warten.");
	    	   if(auswahl == 0){return zuZahlenderBetrag;}
	       }
	       }
	       catch(Exception e){
	    	   System.out.println("Herzlichen Gl�ckwunsch! Sie haben das Programm zerschossen. Rufen Sie bitte jemanden, der lesen kann!");
	    	   System.exit(1);
	       }
	       }
		return zuZahlenderBetrag;
	}
	
	
	}//public class


