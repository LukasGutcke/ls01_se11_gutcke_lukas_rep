
public class Ausgabekonsole {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("�bung 1");
		System.out.println("Aufgabe1:");
		System.out.print("Programmieren ist sch�n. ");
		System.out.print("\nMir f�llt nichts anderes ein.");	
		//Print ist ein Befehl, um einen String oder Variable direkt auszugeben. Der println-Befehl tut genau das selbe, nur das er noch einen Zeilenumbruch einf�gt.
		
		System.out.println(" ");
		System.out.println("Aufgabe2:");
		System.out.println("        *");
		System.out.println("       ***");
		System.out.println("      *****");
		System.out.println("     *******");
		System.out.println("    *********");
		System.out.println("   ***********");
		System.out.println("  *************");
		System.out.println(" ***************");
		System.out.println("       ***");
		System.out.println("       ***");
		
		System.out.println("Aufgabe3:");
		System.out.printf("%.2f %n%.2f %n%.2f %n%.2f %n%.2f %n", 22.4234234, 111.2222, 4.0, 1000000.551, 97.34);
		
		System.out.println("\n�bung 2");
		System.out.println("Aufgabe 1:");
		System.out.printf("%50s%n", "**");
		System.out.printf("%46s%7s%n", "*", "*");
		System.out.printf("%46s%7s%n", "*", "*");
		System.out.printf("%50s%n", "**");
		
		System.out.println("Aufgabe 2:");
		System.out.printf("%-5s=%-19s=%4d\n","0!"," ", 1);
		System.out.printf("%-5s=%-19s=%4d\n","1!"," 1", 1);
		System.out.printf("%-5s=%-19s=%4d\n","2!"," 1 * 2", 2);
		System.out.printf("%-5s=%-19s=%4d\n","3!"," 1 * 2 * 3", 6);
		System.out.printf("%-5s=%-19s=%4d\n","4!"," 1 * 2 * 3 * 4", 24);
		System.out.printf("%-5s=%-19s=%4d\n","5!"," 1 * 2 * 3 * 4 * 5", 120);
		
		System.out.println("Aufgabe 3:");
		System.out.printf("%-12s%10s\n", "Fahrenheit", "Celsius");
		System.out.printf("%-12d%10.2f\n", -20, -28.8889);
		System.out.printf("%-12d%10.2f\n", -10, -23.3333);
		System.out.printf("%-12d%10.2f\n", 0, -17.7778);
		System.out.printf("%-12d%10.2f\n", 20, -6.6667);
		System.out.printf("%-12d%10.2f\n", 30, -1.1111);
	}

}
