import java.util.Scanner;  
public class Makler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double laenge = 0, breite = 0, zimmergroeße = 0;
		
		Scanner einlesen = new Scanner (System.in);
		
		System.out.println("Länge des Raumes: ");
		laenge = einlesen.nextDouble();
		System.out.println("Breite des Raumes: ");
		breite = einlesen.nextDouble();
		
		zimmergroeße = laenge * breite;
		System.out.printf("\n%s%.2f%s", "Zimmergröße: ", zimmergroeße, " qm");
		einlesen.close();
	}

}
