import java.util.Scanner;
public class Uebungen_Methoden {
	static Scanner tastatur = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float kontostand = 100f;
		char auswahl;
		begruessung();
		System.out.printf("\n%s%.2f%s\n", "Ihr derzeitigere Kontostand betr�gt " ,kontostand, "�");
		do {
		System.out.println("Dr�cken Sie 'e', um Geld einzuzahlen oder 'a', um Geld abzuheben");
		auswahl = tastatur.next().charAt(0);
		if(auswahl == 'e') {
			einzahlen(kontostand);
		}
		if(auswahl == 'a') {
			abheben(kontostand);
		}

		if((auswahl != 'a') && (auswahl != 'e')) 
		{
			System.out.println("Sie haben eine falsche Eingabe get�tigt!");
		}
		}while(true);
		//tastatur.close();
	}

	public static void begruessung()
	{
		System.out.println("Guten Tag Herr/Frau...");
	}
	
	public static float einzahlen(float kontostand)
	{
		System.out.println("Wie viel Geld wollen Sie einzahlen?");
		float betrag = tastatur.nextFloat();
		kontostand += betrag;
		System.out.printf("\n%s%.2f%s\n", "Ihr neuer Kontostand betr�gt " ,kontostand, "�");
		return kontostand;
	}
	
	public static float abheben(float kontostand)
	{
		System.out.println("Wie viel Geld wollen Sie abheben?");
		float betrag = tastatur.nextFloat();
		kontostand = kontostand - betrag;
		System.out.printf("\n%s%.2f%s\n", "Ihr neuer Kontostand betr�gt " ,kontostand, "�");
		return kontostand;
	}
}
